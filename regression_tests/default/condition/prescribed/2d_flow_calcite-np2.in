#Description: 1D variably saturated flow problem

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE TH
    /
    SUBSURFACE_TRANSPORT transport
      MODE GIRT
    /
  /
END

SUBSURFACE

#=========================== numerical methods ================================

#=========================== regression =======================================
REGRESSION
  CELL_IDS
    28
    48
    60
    78
  /
END

#=========================== fluid properties =================================
EOS WATER
  DENSITY IF97
  VISCOSITY DEFAULT
END

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED
  NXYZ 10 1 10
  BOUNDS
    0.d0 0.d0 0.d0
    10.d0 1.d0 10.d0
  /
END

#=========================== material properties ==============================
MATERIAL_PROPERTY  soil
  ID 1
  CHARACTERISTIC_CURVES cc1
  POROSITY 0.25
  TORTUOSITY 0.5
  ROCK_DENSITY 2650.d0 kg/m^3
  THERMAL_CONDUCTIVITY_DRY 0.6d0 W/m-C
  THERMAL_CONDUCTIVITY_WET 1.9d0 W/m-C
  HEAT_CAPACITY 830.d0 J/kg-C
  PERMEABILITY
    PERM_X 1.1d-12
    PERM_Y 1.d-12
    PERM_Z 1.d-12
  /
/

MATERIAL_PROPERTY  impermeable
  ID 2
  CHARACTERISTIC_CURVES cc1
  POROSITY 0.15
  TORTUOSITY 0.25
  ROCK_DENSITY 2850.d0
  THERMAL_CONDUCTIVITY_DRY 0.1d0
  THERMAL_CONDUCTIVITY_WET 0.2d0
  HEAT_CAPACITY 850.d0
  PERMEABILITY
    PERM_X 1.d-15
    PERM_Y 1.1d-15
    PERM_Z 1.1d-15
  /
/

#=========================== characteristic curves ============================
CHARACTERISTIC_CURVES cc1
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA  1.d-4
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
END

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    H+
    HCO3-
    Ca++
  /
  SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
    CaCO3(aq)
    CaHCO3+
#    CaOH+ ! requires isotherm @ 25C
  /
  PASSIVE_GAS_SPECIES
    CO2(g)
  /
  MINERALS
    Calcite
  /
  MINERAL_KINETICS
    Calcite
      RATE_CONSTANT 1.d-6
    /
  /
  DATABASE ../../../../database/hanford.dat
  LOG_FORMULATION
  ACTIVITY_COEFFICIENTS TIMESTEP
  OUTPUT
    PH
    TOTAL
    FREE_ION
    ALL
  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 0.3 y
  INITIAL_TIMESTEP_SIZE 1.d0 s
  MAXIMUM_TIMESTEP_SIZE 0.05 y
END

#=========================== output options ===================================
skip
OUTPUT
  SNAPSHOT_FILE
    PERIODIC TIME 0.05 y
    FORMAT HDF5
  /
  OBSERVATION_FILE
    PERIODIC TIMESTEP 1
  /
  VARIABLES
    LIQUID_PRESSURE
    TEMPERATURE
  /
END
noskip

#=========================== observations =====================================
OBSERVATION
  REGION one-quarter
END

OBSERVATION
  REGION middle
END

OBSERVATION
  REGION three-quarter
END

OBSERVATION
  REGION middle-right
END

#=========================== regions ==========================================
REGION all
  INFINITE
END

REGION west
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 1.d0 10.d0
  /
END

REGION east
  FACE EAST
  COORDINATES
    10.d0 0.d0 0.d0
    10.d0 1.d0 10.d0
  /
END

REGION impermeable_zone
  COORDINATES
    3.d0 0.d0 3.d0
    6.d0 1.d0 6.d0
  /
/

REGION well
  COORDINATE 4.5d0 0.5d0 4.5d0
/

REGION one-quarter
  COORDINATE 7.5d0 0.5d0 2.5d0
END

REGION middle
  COORDINATE 7.5d0 0.5d0 5.d0
END

REGION three-quarter
  COORDINATE 7.5d0 0.5d0 7.5d0
END

REGION middle-right
  COORDINATE 10.d0 0.5d0 5.d0
END

#=========================== flow conditions ==================================
FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE HYDROSTATIC
    TEMPERATURE DIRICHLET
  /
  DATUM LIST
    TIME_UNITS y
    0. 0. 0. 10.
    0.25 0. 0. 12.
  /
  LIQUID_PRESSURE 2.d5
  TEMPERATURE LIST
    TIME_UNITS y
    0. 20.d0
    0.2 23.d0
  /
END

FLOW_CONDITION injection
  TYPE
    RATE MASS_RATE
    TEMPERATURE DIRICHLET
  /
  RATE LIST
    TIME_UNITS y
    DATA_UNITS kg/d
    0. 50.
    0.25 0.
  /
  TEMPERATURE 15.d0
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 initial
  /
END

TRANSPORT_CONDITION fluctuating
  TYPE ZERO_GRADIENT
  TIME_UNITS y
  CONSTRAINT_LIST
    0.d0 initial
    0.1d0 pH6
    0.2d0 pH7
  /
END

TRANSPORT_CONDITION inlet
  TYPE DIRICHLET_ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 inlet
  /
END

#=========================== constraints ======================================
CONSTRAINT initial
  CONCENTRATIONS
    H+     1.d-8      F
    HCO3-  1.d-3      G  CO2(g)
    Ca++   5.d-4      M  Calcite
  /
  MINERALS
    Calcite 1.d-5 1.d0
  /
END

CONSTRAINT inlet
  CONCENTRATIONS
    H+     5.         P
    HCO3-  1.d-3      T
    Ca++   1.d-6      Z
  /
END

CONSTRAINT pH6
  CONCENTRATIONS
    H+     6.         P
    HCO3-  1.d-3      T
    Ca++   1.d-6      Z
  /
END

CONSTRAINT pH7
  CONCENTRATIONS
    H+     7.         P
    HCO3-  1.d-3      T
    Ca++   1.d-6      Z
  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  FLOW_CONDITION initial
  TRANSPORT_CONDITION initial
  REGION all
END

PRESCRIBED_CONDITION fixed_cells
  FLOW_CONDITION initial
  TRANSPORT_CONDITION fluctuating
  REGION east
END

SOURCE_SINK injection
  FLOW_CONDITION injection
  TRANSPORT_CONDITION inlet
  REGION well
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL soil
END

STRATA
  MATERIAL impermeable
  REGION impermeable_zone
/

END_SUBSURFACE
