#Description: Test UFD_BIOSPHERE process model 

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      MODE NWT
    /
    UFD_BIOSPHERE ufd_biosphere
    /
  /
END

SUBSURFACE
#=========================== numerical methods ========================================

NUMERICAL_METHODS TRANSPORT

  TIMESTEPPER
    NUM_STEPS_AFTER_TS_CUT 2
    DT_FACTOR 2.0 2.0 1.8 1.6 1.4 1.4 1.3 1.3 1.2 1.2 1.1 1.1
  /

  NEWTON_SOLVER
    NWT_ITOL_RELATIVE_UPDATE
      Ra-226  1.0d-1
      I-129  1.0d-1
      Daugh  1.0d-1
    /
    NWT_ITOL_ABSOLUTE_UPDATE
      Ra-226  1.0d-4
      I-129  1.0d-4
      Daugh  1.0d-4
    /
    NWT_ITOL_SCALED_RESIDUAL
      Ra-226  1.D-3
      I-129 1.d-3
      Daugh 1.d-3
    /
    NWT_ITOL_ABSOLUTE_RESIDUAL
      Ra-226  1.D-7
      I-129 1.d-7
      Daugh 1.d-7
    /
    MAXIMUM_NUMBER_OF_ITERATIONS 12
  /

  LINEAR_SOLVER
    SOLVER DIRECT
  /

END
#=========================== chemistry ========================================
NUCLEAR_WASTE_CHEMISTRY
  SPECIES
    NAME Ra-226
    SOLUBILITY 1.d4
    PRECIP_MOLAR_DENSITY 38.61d3
    ELEMENTAL_KD 0.d0
  /
  SPECIES
    NAME I-129
    SOLUBILITY 1.d8
    PRECIP_MOLAR_DENSITY 38.61d3
    ELEMENTAL_KD 0.d0
  /
  SPECIES
    NAME Daugh
    SOLUBILITY 1.d4
    PRECIP_MOLAR_DENSITY 38.61d3
    ELEMENTAL_KD 0.d0
  /
  RADIOACTIVE_DECAY
    # [1/sec]
    1.37d-11 Ra-226 -> Daugh
    1.40d-15 I-129
    0.d0 Daugh
  /

  OUTPUT
    ALL_SPECIES
    ALL_CONCENTRATIONS
    MINERAL_VOLUME_FRACTION
  /
END

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED
  NXYZ 1 1 1
  BOUNDS
    0.d0  0.d0  0.d0
    1.d0 1.d0 1.d0
  END
END

#=========================== fluid properties =================================
FLUID_PROPERTY 
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY domain
  ID 1
  POROSITY 0.25d0
  TORTUOSITY 1.d0
  ROCK_DENSITY 2700 # kg/m3
END

#=========================== output options ===================================

#=========================== times ============================================
TIME
  FINAL_TIME 1.d2 y
  INITIAL_TIMESTEP_SIZE 1.d-6 day
  MAXIMUM_TIMESTEP_SIZE 1.d3 y
END

#=========================== regions ==========================================
REGION all
  BLOCK 1 1 1 1 1 1
END

REGION well
  BLOCK 1 1 1 1 1 1
END

REGION ERB1B_tester1
  BLOCK 1 1 1 1 1 1
END


#=========================== observation points ===============================
OBSERVATION
  REGION all
END

#=========================== source/sinks =====================================
SOURCE_SINK well
  REGION well
  TRANSPORT_CONDITION sink
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION groundwater
  TYPE DIRICHLET_ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 groundwater
  /
END

TRANSPORT_CONDITION sink
  TYPE DIRICHLET # dummy concentration assigned for outflow
  CONSTRAINT_LIST
    0.d0 groundwater
  /
END

#=========================== transport constraints ============================
CONSTRAINT groundwater
  CONCENTRATIONS
    Ra-226   1.213d-13     AQ   ! total aqueous in mol/m^3 confirmed
    I-129    1.d-9         AQ  
    Daugh    1.d-47        AQ
  /
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION groundwater
  REGION all
END


#=========================== stratigraphy couplers ============================
STRATA
  MATERIAL domain
  REGION all
END


END_SUBSURFACE

#========================== ufd biosphere =====================================
UFD_BIOSPHERE
  
  ERB_1A A_model1
    REGION well
    INDIVIDUAL_CONSUMPTION_RATE 2.d0 L/day
    INCLUDE_UNSUPPORTED_RADS
  /
  ERB_1B B_model1
    REGION ERB1B_tester1
    DILUTION_FACTOR 1.0
    INDIVIDUAL_CONSUMPTION_RATE 2.d0 L/day
    INCLUDE_UNSUPPORTED_RADS
  /
  
  SUPPORTED_RADIONUCLIDES
    RADIONUCLIDE Ra-226
      ELEMENT_KD 5.d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 1.37d-11 1/sec
      INGESTION_DOSE_COEF 2.8d-7 Sv/Bq
    /
    RADIONUCLIDE I-129 
      ELEMENT_KD 0.d0  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 1.40d-15 1/sec
      INGESTION_DOSE_COEF 1.1d-7 Sv/Bq
    /
  /
  
  UNSUPPORTED_RADIONUCLIDES
    RADIONUCLIDE Rn-222
      ELEMENT_KD 0.d0  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 2.1d-6 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 3.5d-9 Sv/Bq
      EMANATION_FACTOR 4.d-1  ! Olszewska-Wasiolek and Arnold 2011
    /
    RADIONUCLIDE Pb-214
      ELEMENT_KD 2.7d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 4.039d-4 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 1.4d-10 Sv/Bq
    /
    RADIONUCLIDE Bi-214
      ELEMENT_KD 1.0d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 5.805d-4 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 1.1d-10 Sv/Bq
    /
    RADIONUCLIDE Pb-210
      ELEMENT_KD 2.7d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 9.85d-10 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 6.9d-7  Sv/Bq
    /
    RADIONUCLIDE Bi-210
      ELEMENT_KD 1.0d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 1.601d-6 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 1.3d-9  Sv/Bq
    /
    RADIONUCLIDE Po-210
      ELEMENT_KD 1.5d2  # L-water/kg-solid average in vicinity of well screen
      DECAY_RATE 5.797d-8 1/sec
      SUPPORTED_PARENT Ra-226
      INGESTION_DOSE_COEF 1.2d-6 Sv/Bq
    /
  /
  
  OUTPUT_START_TIME 1.d0 yr
  
END


