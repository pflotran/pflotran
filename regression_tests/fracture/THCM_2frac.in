#Regression test for fracture model
#3D
#Thermo-hydrological chemical mechanical
#includes individual fractures only (no fracture family)
#granite system
#update permeability, porosity, tortuosity (knt) function included

#================================================

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE TH
    /
 SUBSURFACE_TRANSPORT transport
      MODE GIRT
    END
    
    FRACTURE_MODEL frac01
    END
/
END

SUBSURFACE

#=========================== discretization ===================================
GRID
  TYPE STRUCTURED
  Origin 0.d0  0.d0 -500.0d0
  NXYZ 3 2 3
  DXYZ
    3@1
    2@0.5
    3@3
  /
END

#===========================  flow solver options ===================================

NUMERICAL_METHODS FLOW

  TIMESTEPPER
    TIMESTEP_REDUCTION_FACTOR 2.0d0
    TIMESTEP_MAXIMUM_GROWTH_FACTOR 2.2d0
    MAXIMUM_CONSECUTIVE_TS_CUTS 10
    INITIAL_TIMESTEP_SIZE 50.d0 s
    MAXIMUM_TIMESTEP_SIZE 0.5d0 d
  /

  NEWTON_SOLVER
    VERBOSE_LOGGING
  /

  LINEAR_SOLVER
    SOLVER ITERATIVE
  /

/
 

 NUMERICAL_METHODS TRANSPORT

  LINEAR_SOLVER
    SOLVER ITERATIVE
  /

END


#=========================== times ============================================
TIME
  FINAL_TIME 0.4d0 y
END

#=========================== regression =======================================
REGRESSION
  CELL_IDS
    2
    11
    18
  /
END

#=========================== output options ===================================

OUTPUT
  VELOCITY_AT_CENTER
  SNAPSHOT_FILE
    
    PERIODIC TIME 0.10 y between 0.0 y and 1.0 y
  /
    FORMAT HDF5
    VARIABLES
      TEMPERATURE
      LIQUID_PRESSURE
      PERMEABILITY
      MINERAL_POROSITY #m^3 mineral/m^3 bulk
      LIQUID_SATURATION
      LIQUID_MOBILITY #(m/s/kg)
      MATERIAL_ID
      PERMEABILITY_X
      PERMEABILITY_Y
      PERMEABILITY_Z
  /
END

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    H+
    HCO3-
    Ca++
    SiO2(aq)

  /
  SECONDARY_SPECIES
    OH-
    CO3--
    CO2(aq)
    CaCO3(aq)

  /
  PASSIVE_GAS_SPECIES
   CO2(g)
  /
  MINERALS
    Calcite
    Quartz

   
  /
  
   MINERAL_KINETICS
    Calcite
      PREFACTOR
        RATE_CONSTANT 1.54d-6 mol/m^2-sec  #logk = -5.81
        ACTIVATION_ENERGY 23.5
      /
      PREFACTOR
        RATE_CONSTANT 0.5d0 mol/m^2-sec  #logk = -0.3
        ACTIVATION_ENERGY 14.4d0
        PREFACTOR_SPECIES H+
          ALPHA 1.0d0
        /
      /
      PREFACTOR
        RATE_CONSTANT 3.31d-4 mol/m^2-sec #logk = -3.48
        ACTIVATION_ENERGY 35.4
        PREFACTOR_SPECIES CO2(aq)
          ALPHA 1.0d0
        /
      /
      
      /

      Quartz
      RATE_CONSTANT 0.d0
        /
      
 / 

  DATABASE ../../database/geothermal-hpt.dat
  GEOTHERMAL_HPT
  LOG_FORMULATION
  UPDATE_PERMEABILITY
  UPDATE_POROSITY
  UPDATE_TORTUOSITY
  #OPERATOR_SPLITTING
  ACTIVITY_COEFFICIENTS TIMESTEP
  OUTPUT
    PH
    TOTAL
    ALL
    Calcite
    CO2(aq)
    Quartz

  /
END

!=========================== geothermal fracture model ========================
GEOTHERMAL_FRACTURE_MODEL
  
  THERMAL_EXPANSION_COEFFICIENT 40.0d-6 # [1/C]

  FRACTURE
    ID 1
    HYDRAULIC_APERTURE 3.d-3 # [m]
    CENTER 1.5d0 0.5d0 -495.0d0 # [m]
    NORMAL_VECTOR 1.0d0 0.0d0 0.0d0 # [m]
    RADIUS_X 2.0d0 # [m]
    RADIUS_Y 0.25d0 # [m]
    RADIUS_Z 5.0d0 # [m]
  /
  

   FRACTURE
    ID 2
    HYDRAULIC_APERTURE 3.d-3 # [m]
    CENTER 1.5d0 0.5d0 -496.5d0 # [m]
    NORMAL_VECTOR 0.0d0 0.0d0 1.0d0 # [m]
    RADIUS_X 3.0d0 # [m]
    RADIUS_Y 0.5d0 # [m]
    RADIUS_Z 5.0d0 # [m]
   /
  
/


#=========================== equation of state ================================
EOS WATER
  DENSITY IF97
  ENTHALPY IF97
  STEAM_DENSITY IF97
  STEAM_ENTHALPY IF97
END

#=========================== regions ==========================================

REGION all
  COORDINATES
    -1.d20 -1.d20 -1.d20
     1.d20  1.d20  1.d20
  /
END

REGION bottom_face
  FACE WEST
  COORDINATES
    0.0d0 -1.d20 -500.d0
    3.0d0  1.d20 -500.d0
  /
END

REGION top_face
  FACE EAST
  COORDINATES
    0.0d0 -1.d20 -491.d0
    3.0d0  1.d20 -491.d0
  /
END

#=========================== material properties ==============================
MATERIAL_PROPERTY rock1 # 0 to -2500 m
  ID 1
  CHARACTERISTIC_CURVES rock1
  POROSITY 0.01d0 #not being read
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.8E3
  SPECIFIC_HEAT 0.9d3
  THERMAL_CONDUCTIVITY_DRY 0.1 W/m-C
  THERMAL_CONDUCTIVITY_WET 0.1 W/m-C
  PERMEABILITY_CRITICAL_POROSITY 0.00001
  PERMEABILITY_POWER 0.2
  PERMEABILITY_MIN_SCALE_FACTOR 1.0d-30
  PERMEABILITY
    PERM_X 1.0d-12
    PERM_Y 1.0d-12
    PERM_Z 1.0d-16
  /
END


CHARACTERISTIC_CURVES rock1
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA 5.d-8
    M 0.6 
    LIQUID_RESIDUAL_SATURATION 0.0d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    M 0.6 
    LIQUID_RESIDUAL_SATURATION 0.0d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    M 0.6 
    LIQUID_RESIDUAL_SATURATION 0.0d0
    GAS_RESIDUAL_SATURATION 0.0d0
  /
END


#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END


#=========================== flow conditions ==================================

FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE HYDROSTATIC
    TEMPERATURE dirichlet
  /
  DATUM 0.0 0.0 0.0
  LIQUID_PRESSURE 101325.d0
  TEMPERATURE 150.0d0 # [C]

END


FLOW_CONDITION top_face
  TYPE
   LIQUID_PRESSURE HYDROSTATIC
   TEMPERATURE dirichlet
  /
  TEMPERATURE 150.0d0 # [C]
  
  DATUM 0.0 0.0 0.0
  LIQUID_PRESSURE 101325.d0
 END



#=============================================================================

FLOW_CONDITION Inj_well_1
  TYPE
    RATE MASS_RATE
    TEMPERATURE dirichlet
  /
  SYNC_TIMESTEP_WITH_UPDATE
  RATE 0.00005 kg/s
  TEMPERATURE 60.d0
END


#=========================== transport conditions =============================
TRANSPORT_CONDITION background_conc
  TYPE ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 background_conc
  /
END

TRANSPORT_CONDITION injection_conc
  TYPE ZERO_GRADIENT
  CONSTRAINT_LIST
    0.d0 injection_conc
  /
END

#=========================== constraints ======================================
CONSTRAINT background_conc
  CONCENTRATIONS
    H+     1.d-7      F
    HCO3-  1.d-3     Z 
    Ca++   5.d-4     M  Calcite
    SiO2(aq) 1.d-5   M  Quartz    
  /
  MINERALS   #mineral VF SA
    Calcite   0.04   1.0 m^2/m^3
    Quartz    0.93   1.0 m^2/m^3


  /
END

CONSTRAINT injection_conc
  CONCENTRATIONS
    H+     1.d-4      F
    HCO3-  1.d-10     Z 
    Ca++   1.d-1     F 
    SiO2(aq) 1.d-10   F     

  /
END


#=========================== set initial and bc conditions ====================

INITIAL_CONDITION initial
  REGION all
  FLOW_CONDITION initial
  TRANSPORT_CONDITION background_conc
END


BOUNDARY_CONDITION top
  REGION top_face
  FLOW_CONDITION top_face
  TRANSPORT_CONDITION background_conc
END

#===============================================================================

SOURCE_SINK Inj_well_1
  REGION bottom_face
  FLOW_CONDITION Inj_well_1
  TRANSPORT_CONDITION injection_conc
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL rock1
END

END_SUBSURFACE


